'''Ce fichier contient un programme permettant à l'utilisateur de s'entrainer aux tables de multiplication'''


import random
import time


def main():
    '''Ce programme permet à l'utilisateur de s'entrainer aux tables de multiplication'''
    #initialiser la variable permettant de relancer le programme
    continuer = "O"
    #lancer la boucle permettant de relancer le programme 
    while continuer != "n":
        #initialiser la variable permettant d'éviter deux fois la même question 
        question_precedente = 0
        #initialiser le compteur de bonnes réponses 
        count = 0
        #initialiser le temps de réponse max
        temps_reponse_max = 0.0
        #initialiser la question au temps de réponse max
        question_max = 0
        #initialiser la variable temps total:
        temps_total = 0.0

        
        #faire choisir la table au candidat
        table = (input("Quelle table souhaitez vous réviser ? "))

        #rendre la saisie de table robuste
        while table.isdigit() == False:
            table = (input("Vous devez saisir un nombre ! Quelle table souhaitez vous réviser ? "))

        #transformer l'input en integer
        table = int(table)

        #lancer la boucle de 10 questions 
        for _ in range(10):
            #générer une question aléatoirement entre 1 et 10 
            question = random.randint(1, 10)
            #si question similaire à question précédente, regénérer une question 
            while question == question_precedente:  
                question = random.randint(1, 10)
            #mesurer le temps à l'affichage de la question
            temps = time.time()
            #faire répondre le candidat
            reponse = (input(f'Quel est le résultat de {table} fois {question} ? '))
            #rendre la saisie réponse robuste
            while reponse.isdigit() == False:
                reponse = input(f'Vous devez saisir un nombre ! Quel est le résultat de {table} fois {question} ? ')
            #transformer l'input en integer:
            reponse = int(reponse)
            #mesurer le temps entre l'affichage et la réponse du candidat
            temps_reponse = float(time.time() - temps) 


            #dire à l'utilisateur si sa réponse est correct
            if reponse == table*question:
                print("Correct")
                #stocker la réponse de l'utilisateur 
                count += 1 
            else:
                print("Erreur")


            #si temps max dépassé, enregistrer le temps et la question attachée.
            if temps_reponse > temps_reponse_max:
                temps_reponse_max = temps_reponse
                question_max = question

            #actualiser la question précédente 
            question_precedente = question

            #ajouter temps réponse à temps total
            temps_total = temps_total + temps_reponse


        #initialiser une variable resultat_final en fonction des résultats du candidat conservé dans la variable count.
        if count == 10: 
            resultat_final = "Excellent !"
        elif count == 9:
            resultat_final = "Très bien."
        elif count in (7, 8):
            resultat_final = "Bien."
        elif count in (4, 5, 6):
            resultat_final = "Moyen."
        elif count in (1, 2, 3):
            resultat_final = "Il faut retravailler cette table."
        else:
            resultat_final = "Est-ce que tu l'as fais exprès ?"


        #calculer la moyenne des temps de réponse du candidat
        moyenne = float(temps_total/10)


        #arrondir la moyenne à une valeur avec 3 décimales
        moyenne = round(moyenne, 3)
        #créer une variable avec l'arrondi à 3 décimales de la réponse la plus longue.
        reponse_max_arrondi = round(temps_reponse_max, 3)
        #donner ses résultats finaux au candidat
        print(f"\n{resultat_final} Ton temps moyen de réponse est de {moyenne} secondes. \
Ton temps de réponse le plus long était de {reponse_max_arrondi} secondes à la question \
{table} fois {question_max}.")


        #demander si l'utilisateur veut réessayer
        continuer = input("On continue les révisions (o/n)? ")
         #transformer les N en n
        continuer = continuer.lower()


#lancement de la fonction
main()
